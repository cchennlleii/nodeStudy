var express = require('express');
const fs = require("fs");
let app = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
const port = 3030;


var urlencodedParser = bodyParser.urlencoded({ extended: true });
app.use(cookieParser('ruidoc'));
app.use(urlencodedParser);


app.get("/", function (req, res){
    console.log(`request url is ${req.headers.host}`);
    let html = fs.readFileSync("./content-sercuity-policy.html", "utf-8");
    res.setHeader("Content-Security-Policy", "default-src http: https:; report-uri /report");//指定report发送的地址
    res.send(html);
})

app.post("/report", function (req, res){
    console.log(req.body);
    res.json(req.body);
})

let server = app.listen(port, function () {
    let host = server.address().address;
    let port = server.address().port;
    console.log('Example app listening at http://%s:%s', host, port);
});